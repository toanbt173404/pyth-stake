import { Idl, IdlTypes } from "@coral-xyz/anchor";
import { PublicKey } from "@solana/web3.js";
import { Staking } from "../idl/types/staking";
import * as idljs from "@coral-xyz/anchor/dist/cjs/coder/borsh/idl";
import { IdlTypeDef } from "@coral-xyz/anchor/dist/cjs/idl";
import { wasm } from "./StakeConnection";

export type Position = IdlTypes<Staking>["Position"];
export class PositionAccountJs {
  public owner: PublicKey;
  public positions: Position[];

  constructor(buffer: Buffer, idl: Idl) {
    // Fabricate a fake IDL for this so that we can leverage Anchor's Borsh decoding
    const optionPositionType = {
      name: "OptionPosition",
      type: {
        kind: "struct",
        fields: [{ name: "val", type: { option: { defined: "Position" } } }],
      },
    };
    const optionPositionLayout = idljs.IdlCoder.typeDefLayout(
      optionPositionType as unknown as IdlTypeDef,
      idl.types
    );
    let i = 0;
    const discriminator = buffer.slice(i, i + 8);
    i += 8;
    this.owner = new PublicKey(buffer.slice(i, i + 32));
    i += 32;
    this.positions = [];
    for (let j = 0; j < wasm.Constants.MAX_POSITIONS(); j++) {
      this.positions.push(optionPositionLayout.decode(buffer, i).val);
      i += wasm.Constants.POSITION_BUFFER_SIZE();
    }
  }
}
