import { DirectSecp256k1HdWallet, OfflineSigner } from "@cosmjs/proto-signing";
import {
  assertIsDeliverTxSuccess,
  SigningStargateClient,
  StdFee,
  calculateFee,
  GasPrice,
  coin,
} from "@cosmjs/stargate";
(async () => {
  const wallet = await createAddress();

  await sendTransaction(wallet);
})();

async function createAddress(): Promise<OfflineSigner> {
  const mnemonic =
    "quantum jewel measure broccoli stick search fiction dry glass large pigeon carry foot hospital mimic tooth unaware filter relief veteran chest jealous electric two";
  const wallet = await DirectSecp256k1HdWallet.fromMnemonic(mnemonic, {
    prefix: "celestia",
  });

  return wallet;
}

async function sendTransaction(wallet: OfflineSigner) {
  const rpcEndpoint = "https://rpc-celestia-01.stakeflow.io/";
  const client = await SigningStargateClient.connectWithSigner(
    rpcEndpoint,
    wallet
  );
  const delegatorAddress = "celestia1t7eky9qqxmfjavfa8ysssz3kfad4nqjr0rxg2n";
  const validatorAddress =
    "celestiavaloper1r4kqtye4dzacmrwnh6f057p50pdjm8g59tlhhg";

   const [firstAccount] = await wallet.getAccounts();

   let balance = await client.getBalance(firstAccount.address, 'celestia');
   console.log('balance: ', balance);
  const amount = coin(2000000, "utia");
  const defaultGasPrice = GasPrice.fromString("0.2utia");

  const defaultSendFee: StdFee = calculateFee(200000, defaultGasPrice);

  const transaction = await client.delegateTokens(
    delegatorAddress,
    validatorAddress,
    amount,
    defaultSendFee,
    "Staking TIA"
  );
  assertIsDeliverTxSuccess(transaction);
  console.log("Successfully broadcasted:", transaction);
}