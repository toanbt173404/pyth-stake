import { Connection, Keypair, PublicKey, clusterApiUrl } from "@solana/web3.js";
import { StakeConnection } from "./utils/StakeConnection";
import bs58 from "bs58";
import { AnchorProvider, Wallet, setProvider } from "@coral-xyz/anchor";
import NodeWallet from "@coral-xyz/anchor/dist/cjs/nodewallet";
import { STAKING_ADDRESS } from "./utils/contants";
import { PythBalance } from "./utils/PythBalance";

const authorityPubkey = new PublicKey("7pceGYudQeSsf8aZ3y2wyH23k9k4xYcEskKgxJdKcDLg");
const authorityPrivateKey = bs58.decode("4BU3ZSmLa5XhASXM2KZGZyC6KfFzzZDA7dpJcM9aerCfz8hyxMMxsa6TsmEuRjTBcKhFv2UwuAsx5V529kqLFvZi");

const authority = new Keypair({
    publicKey: authorityPubkey.toBytes(),
    secretKey: authorityPrivateKey,
});

async function main() {
    const amount = PythBalance.fromString("100");
    const connection = new Connection(
        "https://few-summer-darkness.solana-mainnet.quiknode.pro/52a0b019e03dfa7ba33baf2a5f06aed8df29ae77/",
        {
          commitment: "confirmed",
        }
      );

    const provider = new AnchorProvider(connection, new NodeWallet(authority), {});
    setProvider(provider);

    const stakeConnection = await StakeConnection.createStakeConnection(
        connection,
        provider.wallet as Wallet,
        STAKING_ADDRESS
    );
    const stakeAccount = await stakeConnection.getMainAccount(new PublicKey("AotdfpYUVE8Dy26X1mmyEXkT8Frdk8SLuQM7mZKu4niS"));
        console.log("stakeAccount: ",stakeAccount);
    
    await stakeConnection.depositAndLockTokens(stakeAccount, amount)
}

main()