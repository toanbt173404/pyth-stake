"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EPOCH_DURATION = exports.PYTH_TOKEN = exports.REALM_ID = exports.PROFILE_ADDRESS = exports.WALLET_TESTER_ADDRESS = exports.STAKING_ADDRESS = exports.GOVERNANCE_ADDRESS = void 0;
const web3_js_1 = require("@solana/web3.js");
function GOVERNANCE_ADDRESS() {
    return new web3_js_1.PublicKey("pytGY6tWRgGinSCvRLnSv4fHfBTMoiDGiCsesmHWM6U");
}
exports.GOVERNANCE_ADDRESS = GOVERNANCE_ADDRESS;
exports.STAKING_ADDRESS = new web3_js_1.PublicKey("pytS9TjG1qyAZypk7n8rw8gfW9sUaqqYyMhJQ4E7JCQ");
exports.WALLET_TESTER_ADDRESS = new web3_js_1.PublicKey("tstPARXbQ5yxVkRU2UcZRbYphzbUEW6t5ihzpLaafgz");
exports.PROFILE_ADDRESS = new web3_js_1.PublicKey("prfmVhiQTN5Spgoxa8uZJba35V1s7XXReqbBiqPDWeJ");
exports.REALM_ID = new web3_js_1.PublicKey("4ct8XU5tKbMNRphWy4rePsS9kBqPhDdvZoGpmprPaug4");
// This one is valid on mainnet only
exports.PYTH_TOKEN = new web3_js_1.PublicKey("HZ1JovNiVvGrGNiiYvEozEVgZ58xaU3RKwX8eACQBCt3");
exports.EPOCH_DURATION = 3600 * 24 * 7;
