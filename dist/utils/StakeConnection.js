"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StakeAccount = exports.VestingAccountState = exports.StakeConnection = exports.wasm = void 0;
const anchor_1 = require("@coral-xyz/anchor");
const web3_js_1 = require("@solana/web3.js");
const staking_json_1 = __importDefault(require("../idl/staking.json"));
const wasm2 = __importStar(require("@pythnetwork/staking-wasm"));
const spl_token_1 = require("@solana/spl-token");
const bn_js_1 = __importDefault(require("bn.js"));
const idljs = __importStar(require("@coral-xyz/anchor/dist/cjs/coder/borsh/idl"));
const spl_governance_1 = require("@solana/spl-governance");
const assert_1 = __importDefault(require("assert"));
const crypto = __importStar(require("crypto"));
const contants_1 = require("./contants");
const PythBalance_1 = require("./PythBalance");
const transaction_1 = require("./transaction");
const PositionAccountJs_1 = require("./PositionAccountJs");
let wasm = wasm2;
exports.wasm = wasm;
class StakeConnection {
    constructor(program, provider, config, configAddress, votingProductMetadataAccount, votingAccountMetadataWasm) {
        this.votingProduct = { voting: {} };
        this.program = program;
        this.provider = provider;
        this.config = config;
        this.configAddress = configAddress;
        this.votingProductMetadataAccount = votingProductMetadataAccount;
        this.governanceAddress = (0, contants_1.GOVERNANCE_ADDRESS)();
        this.votingAccountMetadataWasm = votingAccountMetadataWasm;
    }
    static connect(connection, wallet) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield StakeConnection.createStakeConnection(connection, wallet, contants_1.STAKING_ADDRESS);
        });
    }
    // creates a program connection and loads the staking config
    // the constructor cannot be async so we use a static method
    static createStakeConnection(connection, wallet, stakingProgramAddress) {
        return __awaiter(this, void 0, void 0, function* () {
            const provider = new anchor_1.AnchorProvider(connection, wallet, {});
            const program = new anchor_1.Program(staking_json_1.default, stakingProgramAddress, provider);
            // Sometimes in the browser, the import returns a promise.
            // Don't fully understand, but this workaround is not terrible
            if (wasm.hasOwnProperty("default")) {
                exports.wasm = wasm = yield wasm.default;
            }
            const configAddress = (web3_js_1.PublicKey.findProgramAddressSync([anchor_1.utils.bytes.utf8.encode(wasm.Constants.CONFIG_SEED())], program.programId))[0];
            const config = yield program.account.globalConfig.fetch(configAddress);
            const votingProductMetadataAccount = (web3_js_1.PublicKey.findProgramAddressSync([
                anchor_1.utils.bytes.utf8.encode(wasm.Constants.TARGET_SEED()),
                anchor_1.utils.bytes.utf8.encode(wasm.Constants.VOTING_TARGET_SEED()),
            ], program.programId))[0];
            const votingProductMetadataAccountData = yield program.provider.connection.getAccountInfo(votingProductMetadataAccount);
            const votingAccountMetadataWasm = new wasm.WasmTargetMetadata(votingProductMetadataAccountData.data);
            return new StakeConnection(program, provider, config, configAddress, votingProductMetadataAccount, votingAccountMetadataWasm);
        });
    }
    /** Gets the user's stake account with the most tokens or undefined if it doesn't exist */
    getMainAccount(user) {
        return __awaiter(this, void 0, void 0, function* () {
            const accounts = yield this.getStakeAccounts(user);
            if (accounts.length == 0) {
                return undefined;
            }
            else {
                return accounts.reduce((prev, curr) => {
                    return prev.tokenBalance.lt(curr.tokenBalance) ? curr : prev;
                });
            }
        });
    }
    /** The public key of the user of the staking program. This connection sends transactions as this user. */
    userPublicKey() {
        return this.provider.wallet.publicKey;
    }
    getAllStakeAccountAddresses() {
        return __awaiter(this, void 0, void 0, function* () {
            // Use the raw web3.js connection so that anchor doesn't try to borsh deserialize the zero-copy serialized account
            const allAccts = yield this.provider.connection.getProgramAccounts(this.program.programId, {
                encoding: "base64",
                filters: [
                    { memcmp: this.program.coder.accounts.memcmp("PositionData") },
                ],
            });
            return allAccts.map((acct) => acct.pubkey);
        });
    }
    /** Gets a users stake accounts */
    getStakeAccounts(user) {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield this.program.provider.connection.getProgramAccounts(this.program.programId, {
                encoding: "base64",
                filters: [
                    {
                        memcmp: this.program.coder.accounts.memcmp("PositionData"),
                    },
                    {
                        memcmp: {
                            offset: 8,
                            bytes: user.toBase58(),
                        },
                    },
                ],
            });
            return yield Promise.all(res.map((account) => __awaiter(this, void 0, void 0, function* () {
                console.log('HIII');
                return yield this.loadStakeAccount(account.pubkey);
            })));
        });
    }
    fetchVotingProductMetadataAccount() {
        return __awaiter(this, void 0, void 0, function* () {
            const inbuf = yield this.program.provider.connection.getAccountInfo(this.votingProductMetadataAccount);
            const pm = new wasm.WasmTargetMetadata(inbuf.data);
            return pm;
        });
    }
    fetchPositionAccount(address) {
        return __awaiter(this, void 0, void 0, function* () {
            const inbuf = yield this.program.provider.connection.getAccountInfo(address);
            const stakeAccountPositionsWasm = new wasm.WasmPositionData(inbuf.data);
            const stakeAccountPositionsJs = new PositionAccountJs_1.PositionAccountJs(inbuf.data, this.program.idl);
            return { stakeAccountPositionsWasm, stakeAccountPositionsJs };
        });
    }
    //stake accounts are loaded by a StakeConnection object
    loadStakeAccount(address) {
        return __awaiter(this, void 0, void 0, function* () {
            const { stakeAccountPositionsWasm, stakeAccountPositionsJs } = yield this.fetchPositionAccount(address);
            const metadataAddress = (web3_js_1.PublicKey.findProgramAddressSync([
                anchor_1.utils.bytes.utf8.encode(wasm.Constants.STAKE_ACCOUNT_METADATA_SEED()),
                address.toBuffer(),
            ], this.program.programId))[0];
            const stakeAccountMetadata = (yield this.program.account.stakeAccountMetadataV2.fetch(metadataAddress)); // TS complains about types. Not exactly sure why they're incompatible.
            const vestingSchedule = StakeAccount.serializeVesting(stakeAccountMetadata.lock, this.program.idl);
            const custodyAddress = (web3_js_1.PublicKey.findProgramAddressSync([
                anchor_1.utils.bytes.utf8.encode(wasm.Constants.CUSTODY_SEED()),
                address.toBuffer(),
            ], this.program.programId))[0];
            const info = yield (0, spl_token_1.getAccount)(this.provider.connection, custodyAddress);
            const tokenBalance = new bn_js_1.default(info.amount.toString());
            const authorityAddress = (web3_js_1.PublicKey.findProgramAddressSync([
                anchor_1.utils.bytes.utf8.encode(wasm.Constants.AUTHORITY_SEED()),
                address.toBuffer(),
            ], this.program.programId))[0];
            const votingAccountMetadataWasm = yield this.fetchVotingProductMetadataAccount();
            return new StakeAccount(address, stakeAccountPositionsWasm, stakeAccountPositionsJs, stakeAccountMetadata, tokenBalance, authorityAddress, vestingSchedule, votingAccountMetadataWasm, this.config);
        });
    }
    // Gets the current unix time, as would be perceived by the on-chain program
    getTime() {
        return __awaiter(this, void 0, void 0, function* () {
            // This is a hack, we are using this deprecated flag to flag whether we are using the mock clock or not
            if (this.config.freeze) {
                // On chain program using mock clock, so get that time
                const updatedConfig = yield this.program.account.globalConfig.fetch(this.configAddress);
                return updatedConfig.mockClockTime;
            }
            else {
                // Using Sysvar clock
                const clockBuf = yield this.program.provider.connection.getAccountInfo(web3_js_1.SYSVAR_CLOCK_PUBKEY);
                return new bn_js_1.default(wasm.getUnixTime(clockBuf.data).toString());
            }
        });
    }
    // Unlock a provided token balance
    unlockTokens(stakeAccount, amount) {
        return __awaiter(this, void 0, void 0, function* () {
            let lockedSummary = stakeAccount.getBalanceSummary(yield this.getTime()).locked;
            if (amount
                .toBN()
                .gt(lockedSummary.locked.toBN().add(lockedSummary.locking.toBN()))) {
                throw new Error("Amount greater than locked amount.");
            }
            yield this.unlockTokensUnchecked(stakeAccount, amount);
        });
    }
    // Unchecked unlock
    unlockTokensUnchecked(stakeAccount, amount) {
        return __awaiter(this, void 0, void 0, function* () {
            const positions = stakeAccount.stakeAccountPositionsJs.positions;
            const time = yield this.getTime();
            const currentEpoch = time.div(this.config.epochDuration);
            const sortPositions = positions
                .map((value, index) => {
                return { index, value };
            })
                .filter((el) => el.value) // position not null
                .filter((el // position is voting
            ) => stakeAccount.stakeAccountPositionsWasm.isPositionVoting(el.index))
                .filter((el // position locking or locked
            ) => [wasm.PositionState.LOCKED, wasm.PositionState.LOCKING].includes(stakeAccount.stakeAccountPositionsWasm.getPositionState(el.index, BigInt(currentEpoch.toString()), this.config.unlockingDuration)))
                .sort((a, b) => (a.value.activationEpoch.gt(b.value.activationEpoch) ? 1 : -1) // FIFO closing
            );
            let amountBeforeFinishing = amount.toBN();
            let i = 0;
            const toClose = [];
            while (amountBeforeFinishing.gt(new bn_js_1.default(0)) && i < sortPositions.length) {
                if (sortPositions[i].value.amount.gte(amountBeforeFinishing)) {
                    toClose.push({
                        index: sortPositions[i].index,
                        amount: amountBeforeFinishing,
                    });
                    amountBeforeFinishing = new bn_js_1.default(0);
                }
                else {
                    toClose.push({
                        index: sortPositions[i].index,
                        amount: sortPositions[i].value.amount,
                    });
                    amountBeforeFinishing = amountBeforeFinishing.sub(sortPositions[i].value.amount);
                }
                i++;
            }
            const instructions = yield Promise.all(toClose.map((el) => this.program.methods
                .closePosition(el.index, el.amount, this.votingProduct)
                .accounts({
                targetAccount: this.votingProductMetadataAccount,
                stakeAccountPositions: stakeAccount.address,
            })
                .instruction()));
            const transactions = yield (0, transaction_1.batchInstructions)(instructions, this.program.provider);
            yield this.program.provider.sendAll(transactions.map((tx) => {
                return { tx, signers: [] };
            }));
        });
    }
    withUpdateVoterWeight(instructions, stakeAccount, action, remainingAccount) {
        return __awaiter(this, void 0, void 0, function* () {
            const updateVoterWeightIx = this.program.methods
                .updateVoterWeight(action)
                .accounts({
                stakeAccountPositions: stakeAccount.address,
            })
                .remainingAccounts(remainingAccount
                ? [{ pubkey: remainingAccount, isWritable: false, isSigner: false }]
                : []);
            instructions.push(yield updateVoterWeightIx.instruction());
            return {
                voterWeightAccount: (yield updateVoterWeightIx.pubkeys()).voterRecord,
                maxVoterWeightRecord: (yield this.program.methods.updateMaxVoterWeight().pubkeys()).maxVoterRecord,
            };
        });
    }
    withCreateAccount(instructions, owner, vesting = {
        fullyVested: {},
    }) {
        return __awaiter(this, void 0, void 0, function* () {
            const nonce = crypto.randomBytes(16).toString("hex");
            const stakeAccountAddress = yield web3_js_1.PublicKey.createWithSeed(this.userPublicKey(), nonce, this.program.programId);
            instructions.push(web3_js_1.SystemProgram.createAccountWithSeed({
                fromPubkey: this.userPublicKey(),
                newAccountPubkey: stakeAccountAddress,
                basePubkey: this.userPublicKey(),
                seed: nonce,
                lamports: yield this.program.provider.connection.getMinimumBalanceForRentExemption(wasm.Constants.POSITIONS_ACCOUNT_SIZE()),
                space: wasm.Constants.POSITIONS_ACCOUNT_SIZE(),
                programId: this.program.programId,
            }));
            instructions.push(yield this.program.methods
                .createStakeAccount(owner, vesting)
                .accounts({
                stakeAccountPositions: stakeAccountAddress,
                mint: this.config.pythTokenMint,
            })
                .instruction());
            return stakeAccountAddress;
        });
    }
    isLlcMember(stakeAccount) {
        return __awaiter(this, void 0, void 0, function* () {
            return (JSON.stringify(stakeAccount.stakeAccountMetadata.signedAgreementHash) ==
                JSON.stringify(this.config.agreementHash));
        });
    }
    withJoinDaoLlc(instructions, stakeAccountAddress) {
        return __awaiter(this, void 0, void 0, function* () {
            instructions.push(yield this.program.methods
                .joinDaoLlc(this.config.agreementHash)
                .accounts({
                stakeAccountPositions: stakeAccountAddress,
            })
                .instruction());
        });
    }
    buildCloseInstruction(stakeAccountPositionsAddress, index, amount) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.program.methods
                .closePosition(index, amount, this.votingProduct)
                .accounts({
                targetAccount: this.votingProductMetadataAccount,
                stakeAccountPositions: stakeAccountPositionsAddress,
            })
                .instruction();
        });
    }
    buildTransferInstruction(stakeAccountPositionsAddress, amount) {
        return __awaiter(this, void 0, void 0, function* () {
            const pythMint = new web3_js_1.PublicKey("HZ1JovNiVvGrGNiiYvEozEVgZ58xaU3RKwX8eACQBCt3");
            const from_account = yield (0, spl_token_1.getAssociatedTokenAddress)(pythMint, this.provider.wallet.publicKey, true);
            const toAccount = (web3_js_1.PublicKey.findProgramAddressSync([
                anchor_1.utils.bytes.utf8.encode(wasm.Constants.CUSTODY_SEED()),
                stakeAccountPositionsAddress.toBuffer(),
            ], this.program.programId))[0];
            const ix = (0, spl_token_1.createTransferInstruction)(from_account, toAccount, this.provider.wallet.publicKey, amount.toNumber());
            return ix;
        });
    }
    hasGovernanceRecord(user) {
        return __awaiter(this, void 0, void 0, function* () {
            const voterAccountInfo = yield this.program.provider.connection.getAccountInfo(yield this.getTokenOwnerRecordAddress(user));
            return Boolean(voterAccountInfo);
        });
    }
    /**
     * Locks all unvested tokens in governance
     */
    lockAllUnvested(stakeAccount) {
        return __awaiter(this, void 0, void 0, function* () {
            const vestingAccountState = stakeAccount.getVestingAccountState(yield this.getTime());
            if (vestingAccountState !=
                VestingAccountState.UnvestedTokensPartiallyLocked &&
                vestingAccountState != VestingAccountState.UnvestedTokensFullyUnlocked) {
                throw Error(`Unexpected account state ${vestingAccountState}`);
            }
            const balanceSummary = stakeAccount.getBalanceSummary(yield this.getTime());
            yield this.lockTokens(stakeAccount, balanceSummary.unvested.unlocked);
        });
    }
    /**
     * Locks the specified amount of tokens in governance.
     */
    lockTokens(stakeAccount, amount) {
        return __awaiter(this, void 0, void 0, function* () {
            const owner = stakeAccount.stakeAccountMetadata.owner;
            const amountBN = amount.toBN();
            const transaction = new web3_js_1.Transaction();
            if (!(yield this.hasGovernanceRecord(owner))) {
                yield (0, spl_governance_1.withCreateTokenOwnerRecord)(transaction.instructions, this.governanceAddress, spl_governance_1.PROGRAM_VERSION_V2, this.config.pythGovernanceRealm, owner, this.config.pythTokenMint, owner);
            }
            if (!(yield this.isLlcMember(stakeAccount))) {
                yield this.withJoinDaoLlc(transaction.instructions, stakeAccount.address);
            }
            transaction.instructions.push(yield this.program.methods
                .createPosition(this.votingProduct, amountBN)
                .accounts({
                stakeAccountPositions: stakeAccount.address,
                targetAccount: this.votingProductMetadataAccount,
            })
                .instruction());
            yield this.provider.sendAndConfirm(transaction);
        });
    }
    setupVestingAccount(amount, owner, vestingSchedule, transfer = true) {
        return __awaiter(this, void 0, void 0, function* () {
            const transaction = new web3_js_1.Transaction();
            //Forgive me, I didn't find a better way to check the enum variant
            if (vestingSchedule.periodicVestingAfterListing) {
                (0, assert_1.default)(vestingSchedule.periodicVestingAfterListing.initialBalance);
                (0, assert_1.default)(vestingSchedule.periodicVestingAfterListing.initialBalance.lte(amount.toBN()));
            }
            else if (vestingSchedule.periodicVesting) {
                (0, assert_1.default)(vestingSchedule.periodicVesting.initialBalance);
                (0, assert_1.default)(vestingSchedule.periodicVesting.initialBalance.lte(amount.toBN()));
            }
            const stakeAccountAddress = yield this.withCreateAccount(transaction.instructions, owner, vestingSchedule);
            if (transfer) {
                transaction.instructions.push(yield this.buildTransferInstruction(stakeAccountAddress, amount.toBN()));
            }
            yield this.provider.sendAndConfirm(transaction, []);
        });
    }
    depositTokens(stakeAccount, amount) {
        return __awaiter(this, void 0, void 0, function* () {
            let stakeAccountAddress;
            const owner = this.provider.wallet.publicKey;
            const ixs = [];
            const signers = [];
            if (!stakeAccount) {
                stakeAccountAddress = yield this.withCreateAccount(ixs, owner);
            }
            else {
                stakeAccountAddress = stakeAccount.address;
            }
            if (!(yield this.hasGovernanceRecord(owner))) {
                yield (0, spl_governance_1.withCreateTokenOwnerRecord)(ixs, this.governanceAddress, spl_governance_1.PROGRAM_VERSION_V2, this.config.pythGovernanceRealm, owner, this.config.pythTokenMint, owner);
            }
            if (!stakeAccount || !(yield this.isLlcMember(stakeAccount))) {
                yield this.withJoinDaoLlc(ixs, stakeAccountAddress);
            }
            ixs.push(yield this.buildTransferInstruction(stakeAccountAddress, amount.toBN()));
            const tx = new web3_js_1.Transaction();
            tx.add(...ixs);
            yield this.provider.sendAndConfirm(tx, signers);
        });
    }
    getTokenOwnerRecordAddress(user) {
        return __awaiter(this, void 0, void 0, function* () {
            return (0, spl_governance_1.getTokenOwnerRecordAddress)(this.governanceAddress, this.config.pythGovernanceRealm, this.config.pythTokenMint, user);
        });
    }
    // Unlock all vested and unvested tokens
    unlockAll(stakeAccount) {
        return __awaiter(this, void 0, void 0, function* () {
            const vestingAccountState = stakeAccount.getVestingAccountState(yield this.getTime());
            if (vestingAccountState != VestingAccountState.UnvestedTokensFullyLocked &&
                vestingAccountState !=
                    VestingAccountState.UnvestedTokensPartiallyLocked &&
                vestingAccountState !=
                    VestingAccountState.UnvestedTokensFullyLockedExceptCooldown) {
                throw Error(`Unexpected account state ${vestingAccountState}`);
            }
            const balanceSummary = stakeAccount.getBalanceSummary(yield this.getTime());
            const amountBN = balanceSummary.locked.locked
                .toBN()
                .add(balanceSummary.locked.locking.toBN())
                .add(balanceSummary.unvested.locked.toBN())
                .add(balanceSummary.unvested.locking.toBN());
            const amount = new PythBalance_1.PythBalance(amountBN);
            yield this.unlockTokensUnchecked(stakeAccount, amount);
        });
    }
    depositAndLockTokens(stakeAccount, amount) {
        return __awaiter(this, void 0, void 0, function* () {
            let stakeAccountAddress;
            const owner = this.provider.wallet.publicKey;
            const ixs = [];
            const signers = [];
            if (!stakeAccount) {
                stakeAccountAddress = yield this.withCreateAccount(ixs, owner);
            }
            else {
                stakeAccountAddress = stakeAccount.address;
                const vestingAccountState = stakeAccount.getVestingAccountState(yield this.getTime());
                if (vestingAccountState != VestingAccountState.UnvestedTokensFullyLocked &&
                    vestingAccountState != VestingAccountState.FullyVested) {
                    throw Error(`Unexpected account state ${vestingAccountState}`);
                }
            }
            if (!(yield this.hasGovernanceRecord(owner))) {
                yield (0, spl_governance_1.withCreateTokenOwnerRecord)(ixs, this.governanceAddress, spl_governance_1.PROGRAM_VERSION_V2, this.config.pythGovernanceRealm, owner, this.config.pythTokenMint, owner);
            }
            if (!stakeAccount || !(yield this.isLlcMember(stakeAccount))) {
                yield this.withJoinDaoLlc(ixs, stakeAccountAddress);
            }
            ixs.push(yield this.buildTransferInstruction(stakeAccountAddress, amount.toBN()));
            if (stakeAccount) {
                // Each of these instructions is 27 bytes (<< 1232) so we don't cap how many of them we fit in the transaction
                ixs.push(...(yield this.buildCleanupUnlockedPositions(stakeAccount))); // Try to make room by closing unlocked positions
            }
            yield this.program.methods
                .createPosition(this.votingProduct, amount.toBN())
                .preInstructions(ixs)
                .accounts({
                stakeAccountPositions: stakeAccountAddress,
                targetAccount: this.votingProductMetadataAccount,
            })
                .signers(signers)
                .rpc({ skipPreflight: true });
        });
    }
    buildCleanupUnlockedPositions(stakeAccount) {
        return __awaiter(this, void 0, void 0, function* () {
            const time = yield this.getTime();
            const currentEpoch = time.div(this.config.epochDuration);
            const unlockedPositions = stakeAccount.stakeAccountPositionsJs.positions
                .map((value, index) => {
                return { index, value };
            })
                .filter((el) => el.value) // position not null
                .filter((el // position is voting
            ) => stakeAccount.stakeAccountPositionsWasm.isPositionVoting(el.index))
                .filter((el // position is unlocked
            ) => stakeAccount.stakeAccountPositionsWasm.getPositionState(el.index, BigInt(currentEpoch.toString()), this.config.unlockingDuration) === wasm.PositionState.UNLOCKED)
                .reverse(); // reverse so that earlier deletions don't affect later ones
            return yield Promise.all(unlockedPositions.map((position) => this.buildCloseInstruction(stakeAccount.address, position.index, position.value.amount)));
        });
    }
    requestSplit(stakeAccount, amount, recipient) {
        return __awaiter(this, void 0, void 0, function* () {
            const preInstructions = [
                web3_js_1.ComputeBudgetProgram.setComputeUnitPrice({ microLamports: 1000 }),
            ];
            preInstructions.push(...(yield this.buildCleanupUnlockedPositions(stakeAccount)));
            yield this.program.methods
                .requestSplit(amount.toBN(), recipient)
                .preInstructions(preInstructions)
                .accounts({
                stakeAccountPositions: stakeAccount.address,
            })
                .rpc();
        });
    }
    getScalingFactor() {
        let currentEpoch = new bn_js_1.default(Date.now() / 1000).div(this.config.epochDuration);
        let currentAmountLocked = Number(this.votingAccountMetadataWasm.getCurrentAmountLocked(BigInt(currentEpoch.toString())));
        return currentAmountLocked / Number(wasm.Constants.MAX_VOTER_WEIGHT());
    }
    getStakerAndAmountFromPositionAccountData(positionAccountData) {
        const positionAccountJs = new PositionAccountJs_1.PositionAccountJs(Buffer.from(positionAccountData), staking_json_1.default);
        const positionAccountWasm = new wasm.WasmPositionData(positionAccountData);
        const time = new bn_js_1.default(Date.now() / 1000);
        const currentEpoch = time.div(this.config.epochDuration);
        const unlockingDuration = this.config.unlockingDuration;
        const currentEpochBI = BigInt(currentEpoch.toString());
        const lockedBalanceSummary = positionAccountWasm.getLockedBalanceSummary(currentEpochBI, unlockingDuration);
        const epochOfFirstStake = positionAccountJs.positions.reduce((prev, curr) => {
            if (!curr) {
                return prev;
            }
            if (!prev) {
                return curr.activationEpoch;
            }
            else {
                return bn_js_1.default.min(curr.activationEpoch, prev);
            }
        }, undefined);
        // Default to the start of the next epoch if there are no positions
        const timeOfFirstStake = (epochOfFirstStake !== null && epochOfFirstStake !== void 0 ? epochOfFirstStake : currentEpoch.add(new bn_js_1.default(1))).mul(this.config.epochDuration);
        return {
            owner: positionAccountJs.owner,
            stakedAmount: new bn_js_1.default(lockedBalanceSummary.locked.toString()).add(new bn_js_1.default(lockedBalanceSummary.preunlocking.toString())),
            timeOfFirstStake,
        };
    }
}
exports.StakeConnection = StakeConnection;
var VestingAccountState;
(function (VestingAccountState) {
    VestingAccountState[VestingAccountState["FullyVested"] = 0] = "FullyVested";
    VestingAccountState[VestingAccountState["UnvestedTokensFullyLocked"] = 1] = "UnvestedTokensFullyLocked";
    VestingAccountState[VestingAccountState["UnvestedTokensFullyLockedExceptCooldown"] = 2] = "UnvestedTokensFullyLockedExceptCooldown";
    VestingAccountState[VestingAccountState["UnvestedTokensPartiallyLocked"] = 3] = "UnvestedTokensPartiallyLocked";
    VestingAccountState[VestingAccountState["UnvestedTokensFullyUnlockedExceptCooldown"] = 4] = "UnvestedTokensFullyUnlockedExceptCooldown";
    VestingAccountState[VestingAccountState["UnvestedTokensFullyUnlocked"] = 5] = "UnvestedTokensFullyUnlocked";
})(VestingAccountState || (exports.VestingAccountState = VestingAccountState = {}));
class StakeAccount {
    constructor(address, stakeAccountPositionsWasm, stakeAccountPositionsJs, stakeAccountMetadata, tokenBalance, authorityAddress, vestingSchedule, // Borsh serialized
    votingAccountMetadataWasm, config) {
        this.address = address;
        this.stakeAccountPositionsWasm = stakeAccountPositionsWasm;
        this.stakeAccountPositionsJs = stakeAccountPositionsJs;
        this.stakeAccountMetadata = stakeAccountMetadata;
        this.tokenBalance = tokenBalance;
        this.authorityAddress = authorityAddress;
        this.vestingSchedule = vestingSchedule;
        this.votingAccountMetadataWasm = votingAccountMetadataWasm;
        this.config = config;
    }
    getBalanceSummary(unixTime) {
        let unvestedBalance = wasm.getUnvestedBalance(this.vestingSchedule, BigInt(unixTime.toString()), this.config.pythTokenListTime
            ? BigInt(this.config.pythTokenListTime.toString())
            : undefined);
        let currentEpoch = unixTime.div(this.config.epochDuration);
        let unlockingDuration = this.config.unlockingDuration;
        let currentEpochBI = BigInt(currentEpoch.toString());
        let withdrawable;
        try {
            withdrawable = this.stakeAccountPositionsWasm.getWithdrawable(BigInt(this.tokenBalance.toString()), unvestedBalance, currentEpochBI, unlockingDuration);
        }
        catch (e) {
            throw Error("This account has less tokens than the unlocking schedule or your staking position requires. Please contact support.");
        }
        const withdrawableBN = new bn_js_1.default(withdrawable.toString());
        const unvestedBN = new bn_js_1.default(unvestedBalance.toString());
        const lockedSummaryBI = this.stakeAccountPositionsWasm.getLockedBalanceSummary(currentEpochBI, unlockingDuration);
        let lockingBN = new bn_js_1.default(lockedSummaryBI.locking.toString());
        let lockedBN = new bn_js_1.default(lockedSummaryBI.locked.toString());
        let preunlockingBN = new bn_js_1.default(lockedSummaryBI.preunlocking.toString());
        let unlockingBN = new bn_js_1.default(lockedSummaryBI.unlocking.toString());
        // For the user it makes sense that all the categories add up to the number of tokens in their custody account
        // This sections corrects the locked balances to achieve this invariant
        let excess = lockingBN
            .add(lockedBN)
            .add(preunlockingBN)
            .add(unlockingBN)
            .add(withdrawableBN)
            .add(unvestedBN)
            .sub(this.tokenBalance);
        let lockedUnvestedBN, lockingUnvestedBN, preUnlockingUnvestedBN, unlockingUnvestedBN;
        // First adjust locked. Most of the time, the unvested tokens are in this state.
        [excess, lockedBN, lockedUnvestedBN] = this.adjustLockedAmount(excess, lockedBN);
        // The unvested tokens can also be in a locking state at the very beginning.
        // The reason why we adjust this balance second is the following
        // If a user has 100 unvested in a locked position and decides to stake 1 free token
        // we want that token to appear as locking
        [excess, lockingBN, lockingUnvestedBN] = this.adjustLockedAmount(excess, lockingBN);
        // Needed to represent vesting accounts unlocking before the vesting event
        [excess, preunlockingBN, preUnlockingUnvestedBN] = this.adjustLockedAmount(excess, preunlockingBN);
        [excess, unlockingBN, unlockingUnvestedBN] = this.adjustLockedAmount(excess, unlockingBN);
        //Enforce the invariant
        (0, assert_1.default)(lockingBN
            .add(lockedBN)
            .add(preunlockingBN)
            .add(unlockingBN)
            .add(withdrawableBN)
            .add(unvestedBN)
            .eq(this.tokenBalance));
        return {
            // withdrawable tokens
            withdrawable: new PythBalance_1.PythBalance(withdrawableBN),
            // vested tokens not currently withdrawable
            locked: {
                locking: new PythBalance_1.PythBalance(lockingBN),
                locked: new PythBalance_1.PythBalance(lockedBN),
                unlocking: new PythBalance_1.PythBalance(unlockingBN),
                preunlocking: new PythBalance_1.PythBalance(preunlockingBN),
            },
            // unvested tokens
            unvested: {
                total: new PythBalance_1.PythBalance(unvestedBN),
                locked: new PythBalance_1.PythBalance(lockedUnvestedBN),
                locking: new PythBalance_1.PythBalance(lockingUnvestedBN),
                unlocking: new PythBalance_1.PythBalance(unlockingUnvestedBN),
                preunlocking: new PythBalance_1.PythBalance(preUnlockingUnvestedBN),
                unlocked: new PythBalance_1.PythBalance(unvestedBN
                    .sub(lockedUnvestedBN)
                    .sub(lockingUnvestedBN)
                    .sub(unlockingUnvestedBN)
                    .sub(preUnlockingUnvestedBN)),
            },
        };
    }
    adjustLockedAmount(excess, locked) {
        if (excess.gt(new bn_js_1.default(0))) {
            if (excess.gte(locked)) {
                return [excess.sub(locked), new bn_js_1.default(0), locked];
            }
            else {
                return [new bn_js_1.default(0), locked.sub(excess), excess];
            }
        }
        else {
            return [new bn_js_1.default(0), locked, new bn_js_1.default(0)];
        }
    }
    static serializeVesting(lock, idl) {
        var _a;
        const VESTING_SCHED_MAX_BORSH_LEN = 4 * 8 + 1;
        let buffer = Buffer.alloc(VESTING_SCHED_MAX_BORSH_LEN);
        let idltype = (_a = idl === null || idl === void 0 ? void 0 : idl.types) === null || _a === void 0 ? void 0 : _a.find((v) => v.name === "VestingSchedule");
        const vestingSchedLayout = idljs.IdlCoder.typeDefLayout(idltype, idl.types);
        const length = vestingSchedLayout.encode(lock, buffer, 0);
        return buffer.slice(0, length);
    }
    getVestingAccountState(unixTime) {
        const vestingSummary = this.getBalanceSummary(unixTime).unvested;
        if (vestingSummary.total.isZero()) {
            return VestingAccountState.FullyVested;
        }
        if (vestingSummary.preunlocking.isZero() &&
            vestingSummary.unlocking.isZero()) {
            if (vestingSummary.locked.isZero() && vestingSummary.locking.isZero()) {
                return VestingAccountState.UnvestedTokensFullyUnlocked;
            }
            else if (vestingSummary.unlocked.isZero()) {
                return VestingAccountState.UnvestedTokensFullyLocked;
            }
            else {
                return VestingAccountState.UnvestedTokensPartiallyLocked;
            }
        }
        else {
            if (vestingSummary.locked.isZero() && vestingSummary.locking.isZero()) {
                return VestingAccountState.UnvestedTokensFullyUnlockedExceptCooldown;
            }
            else if (vestingSummary.unlocked.isZero()) {
                return VestingAccountState.UnvestedTokensFullyLockedExceptCooldown;
            }
            else {
                return VestingAccountState.UnvestedTokensPartiallyLocked;
            }
        }
    }
}
exports.StakeAccount = StakeAccount;
