"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const web3_js_1 = require("@solana/web3.js");
const StakeConnection_1 = require("./utils/StakeConnection");
const bs58_1 = __importDefault(require("bs58"));
const anchor_1 = require("@coral-xyz/anchor");
const nodewallet_1 = __importDefault(require("@coral-xyz/anchor/dist/cjs/nodewallet"));
const contants_1 = require("./utils/contants");
const PythBalance_1 = require("./utils/PythBalance");
const authorityPubkey = new web3_js_1.PublicKey("7pceGYudQeSsf8aZ3y2wyH23k9k4xYcEskKgxJdKcDLg");
const authorityPrivateKey = bs58_1.default.decode("4BU3ZSmLa5XhASXM2KZGZyC6KfFzzZDA7dpJcM9aerCfz8hyxMMxsa6TsmEuRjTBcKhFv2UwuAsx5V529kqLFvZi");
const authority = new web3_js_1.Keypair({
    publicKey: authorityPubkey.toBytes(),
    secretKey: authorityPrivateKey,
});
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        const amount = PythBalance_1.PythBalance.fromString("100");
        const connection = new web3_js_1.Connection("https://few-summer-darkness.solana-mainnet.quiknode.pro/52a0b019e03dfa7ba33baf2a5f06aed8df29ae77/", {
            commitment: "confirmed",
        });
        const provider = new anchor_1.AnchorProvider(connection, new nodewallet_1.default(authority), {});
        (0, anchor_1.setProvider)(provider);
        const stakeConnection = yield StakeConnection_1.StakeConnection.createStakeConnection(connection, provider.wallet, contants_1.STAKING_ADDRESS);
        const stakeAccount = yield stakeConnection.getMainAccount(new web3_js_1.PublicKey("AotdfpYUVE8Dy26X1mmyEXkT8Frdk8SLuQM7mZKu4niS"));
        console.log("stakeAccount: ", stakeAccount);
        yield stakeConnection.depositAndLockTokens(stakeAccount, amount);
    });
}
main();
